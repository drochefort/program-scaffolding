const load = require('load-grunt-tasks');

module.exports = (grunt) => {
    // Load contrib tasks
    load(grunt);

    // Load custom tasks from the tasks folder
    grunt.loadTasks('tasks');

    // Load configuration file
    const config = grunt.file.readJSON('config.json', { encoding: 'utf-8' });

    // Configure tasks
    grunt.initConfig({
        eslint: {
            options: {
                configFile: '.eslintrc.json',
            },
            target: ['Gruntfile.js', 'tasks/*.js'],
        },
        clean: {
            temp: ['temp'],
            assets: ['assets'],
        },
    });

    /**
     * Main Program Scaffolding Task
     */
    grunt.registerTask('scaffold-program',
        () => config.widgets.forEach(name => grunt.task.run([
            'clean:temp',
            `clone-widget:${name}`,
            `build-widget:${name}`,
            `copy-widget:${name}`,
            'clean:temp',
        ])));

    grunt.registerTask('default', [
        'eslint',
        'clean:temp',
        'clean:assets',
        'scaffold-program',
    ]);
};
