/**
 * Build a widget within temp/${name}
 *
 * name: name of the widget provided as parameter
 */
const shell = require('shelljs');
const errorCodes = require('./error-codes');

module.exports = (grunt) => {
    function execute(command) {
        const output = shell.exec(command);
        grunt.log.writeln(output.stdout);
        if (output.stderr) {
            grunt.log.error(output.stderr);
        }
    }

    grunt.registerTask('build-widget',
        'build individual widget in temp/${name}',
        (name) => {
            shell.pushd(`temp/${name}`);

            if (shell.test('-e', 'bower.json')) {
                execute('bower install');
            }

            if (shell.test('-e', 'package.json')) {
                execute('npm install');
                const pack = grunt.file.read('package.json');
                if (pack.scripts && pack.scripts.build) {
                    execute(name, errorCodes.NO_BUILD_SCRIPT);
                }
            } else {
                grunt.log.error(name, errorCodes.NO_PACKAGE_FOUND);
            }

            shell.popd();
        });
};
