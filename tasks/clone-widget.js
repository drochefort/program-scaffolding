/**
 * Clone widget git repositories in temp/${name}
 *
 * name: name of the widget provided as parameter
 */
module.exports = (grunt) => {
    grunt.registerTask('clone-widget',
        'clone individual widget git repository into temp/${name}',
        name => {
            grunt.config.set('gitclone', {
                main: {
                    options: {
                        repository: `git@bitbucket.org:hmheng/${name}.git`,
                        branch: 'develop',
                        directory: `temp/${name}`,
                    },
                },
            });

            grunt.task.run('gitclone');
        });
};
