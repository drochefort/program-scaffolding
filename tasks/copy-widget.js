/**
 * Copy widget release versions from temp/${name}/build
 * to assets/widgets/${name}
 *
 * name: name of the widget provided as parameter
 */
module.exports = (grunt) => {
    grunt.registerTask('copy-widget',
        'move widget from temp/ to assets/widgets/${name}',
        name => {
            grunt.config.set('copy', {
                main: {
                    expand: true,
                    // TODO: replace /tests by /build
                    cwd: `temp/${name}/tests`,
                    src: '**',
                    dest: `assets/widgets/${name}`,
                    filter: 'isFile',
                },
            });

            grunt.task.run('copy');
        });
};
