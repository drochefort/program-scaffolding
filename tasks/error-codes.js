exports.NO_PACKAGE_FOUND = 'package.json not found';
exports.NO_BUILD_SCRIPT = 'npm build script not defined in package.json';