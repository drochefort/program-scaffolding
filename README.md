# Program Scaffolding Example #

This is an example to illustrate how we can automatically scaffold an Habitat program repository from a list of individual widget git repositories.

This example is aligned with current Jenkins Habitat CI Sync Job.

### How do I get set up? ###

You will need Node 4 or above because the scripts are written in ES6.

```
#!bash

npm install -g bower grunt-cli
npm install
npm start
```

### Program Scaffolding Overview

- STEP 1: clone all individual widgets in the temp/ folder
- STEP 2: build all widgets in the temp/ folder
- STEP 3: copy the release version of all widgets to assets/widgets/ folder

### Technology stack

** Automation **

- Node (version 4 or above)
- Grunt 1.0 task runner
